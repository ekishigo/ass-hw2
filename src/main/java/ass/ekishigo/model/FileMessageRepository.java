package ass.ekishigo.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static ass.ekishigo.model.Response.Status.INTERNAL_WARN;

/**
 * Created by igorekishev on 12.03.2017.
 * Project hw2
 *
 * @author Igor Ekishev
 * @email ekishigo@fel.cvut.cz
 */
public class FileMessageRepository extends Observable implements MessageRepository {
    private static final Logger logger = LoggerFactory.getLogger(FileMessageRepository.class.getSimpleName());
    private static final String PERSISTENCE_FAILS_WARN_MSG = "Saving to file not working. Check file.";
    private static final String FILE_READING_FAILS_WARN_MSG = "Can not find from file.";
    private List<String> messages;
    private final File file;

    public FileMessageRepository(File file) {
        super();
        this.file = file;
        this.messages = new ArrayList<>(10);

        if (file.exists()) {
            if (!file.canRead()) {
                System.err.println("Sorry, the file can not be read.");
                System.exit(0);
            }
        }
    }

    @Override
    public boolean add(String message) {
        logger.debug("add new msg");
        boolean added = messages.add(message);
        super.setChanged();
        Response response;
        try {
            persist(message);
            response = new Response(Response.Status.OK, message);
        } catch (IOException e) {
            logger.error("persistence to file fails : {}", e.getMessage());
            response = new Response(INTERNAL_WARN.withMsg(PERSISTENCE_FAILS_WARN_MSG), message);
        }
        notifyObservers(response);
        return added;
    }

    private void persist(String message) throws IOException {
        new PrintWriter(
                new FileWriter(file, true))
                .append(message)
                .append("\n")
                .close();
    }

    private String lastMsg() {
        return messages.get(messages.size() - 1);
    }

    @Override
    public boolean clear() {
        return file.delete();
    }

    @Override
    public void addObserver(Observer chatPresenter) {
        super.addObserver(chatPresenter);
    }

    @Override
    public List<String> getAll() {
        try {
            FileInputStream fis = new FileInputStream(file);
            List<String> ret = new BufferedReader(new InputStreamReader(fis))
                    .lines()
                    .collect(Collectors.toList());
            fis.close();
            return ret;
        } catch (IOException e) {
            logger.error("data retrieving from file fails : {}", e.getMessage());
            Response response = new Response(
                    INTERNAL_WARN.withMsg(FILE_READING_FAILS_WARN_MSG),
                    null);

            // state of the file changed
            super.setChanged();
            notifyObservers(response);
            return Collections.emptyList();
        }
    }
}
