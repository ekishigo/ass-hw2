package ass.ekishigo.model;

import java.util.List;
import java.util.Observer;

/**
 * Created by igorekishev on 12.03.2017.
 * Project hw2
 *
 * @author Igor Ekishev
 * @email ekishigo@fel.cvut.cz
 */
public interface MessageRepository {
    boolean add(String message);

    boolean clear();

    void addObserver(Observer observer);

    List<String> getAll();
}
