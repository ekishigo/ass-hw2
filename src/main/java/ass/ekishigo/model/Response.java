package ass.ekishigo.model;

import java.util.Optional;

/**
 * Created by igorekishev on 12.03.2017.
 * Project hw2
 *
 * @author Igor Ekishev
 * @email ekishigo@fel.cvut.cz
 */
public class Response {
    private final Status status;
    private final String data;

    public Response(Status status, String data) {
        this.status = status;
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public Optional<String> getData() {
        return Optional.ofNullable(data);
    }

    public enum Status {
        OK("OK"),
        INTERNAL_WARN("INTERNAL_WARNING"),
        FATAL_ERROR("FATAL_ERRO");
        private String msg;

        Status(final String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public Status withMsg(String info) {
            this.msg = info;
            return this;
        }

        @Override
        public String toString() {
            return msg;
        }
    }
}
