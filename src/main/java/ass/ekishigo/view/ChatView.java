package ass.ekishigo.view;

import ass.ekishigo.presenter.ChatPresenter;

import java.util.List;
import java.util.Optional;

/**
 * Created by igorekishev on 12.03.2017.
 * Project hw2
 *
 * @author Igor Ekishev
 * @email ekishigo@fel.cvut.cz
 */
public interface ChatView {
    interface Factory {
        ChatView create(ChatPresenter chatPresenter);
    }

    Optional<String> getNewMessageText();

    void clearInputText();

    void appendMessage(String message);

    void setMessages(List<String> messages);
    List<String> getMessages();

    void setWarning(String message);

    void clearWarning();

    void flashSuccess();
}
