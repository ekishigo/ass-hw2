package ass.ekishigo.view;

import ass.ekishigo.presenter.ChatPresenter;
import com.google.inject.assistedinject.Assisted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.empty;

/**
 * Created by igorekishev on 12.03.2017.
 * Project hw2
 *
 * @author Igor Ekishev
 * @email ekishigo@fel.cvut.cz
 */
public class SwingChatView implements ChatView {


    private static final Logger logger = LoggerFactory.getLogger(SwingChatView.class.getSimpleName());
    private final JFrame frame = new JFrame();
    private final JPanel verticalPanel = new JPanel();
    // !!! keep cols of messagesTextArea and newMessageInput same !!!
    private final JTextField warningLabel = new JTextField(80);
    private final JTextArea messagesTextArea = new JTextArea(15, 80);
    private final JTextField newMessageInput = new JTextField(80);
    private final JButton btnSend = new JButton("Send");
    private final JButton btnHistory = new JButton("History");

    private ChatPresenter chatPresenter;

    @Inject
    public SwingChatView(@Assisted final ChatPresenter chatPresenter) throws IOException {
        this.chatPresenter = chatPresenter;
        setupGUI();
    }

    private void setupGUI() {
        setupPanels();
        setupFrame();
        addNewMessageListenerTo(btnSend);
        addShowHistoryListenerTo(btnHistory);
    }

    private void setupPanels() {
        JPanel warningPanel = initWarningPanel();
        initIMessagesTextArea();
        JPanel horizontalPanel = initInputPanel();
        initMainView(warningPanel, horizontalPanel);
    }

    private void initMainView(JPanel warningPanel, JPanel horizontalPanel) {
        verticalPanel.setLayout(new BoxLayout(verticalPanel, BoxLayout.Y_AXIS));
        verticalPanel.add(warningPanel);
        verticalPanel.add(messagesTextArea);
        verticalPanel.add(horizontalPanel);
    }

    private void initIMessagesTextArea() {
        // makes messagesTextArea readonly
        messagesTextArea.setEditable(false);
    }

    private JPanel initInputPanel() {
        JPanel horizontalPanel = new JPanel();
        horizontalPanel.setLayout(new BoxLayout(horizontalPanel, BoxLayout.X_AXIS));
        horizontalPanel.add(newMessageInput);
        horizontalPanel.add(btnSend);
        horizontalPanel.add(btnHistory);
        return horizontalPanel;
    }

    private JPanel initWarningPanel() {
        JPanel warningPanel = new JPanel();
        warningPanel.setLayout(new BoxLayout(warningPanel, BoxLayout.X_AXIS));
        warningLabel.setText("");
        warningLabel.setEditable(false);
        warningPanel.add(warningLabel);
        return warningPanel;
    }

    private void setupFrame() {
        frame.setTitle("Chat window");
        frame.add(verticalPanel);
        frame.setVisible(true);

        // adjusts window to size of components
        frame.pack();

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                e.getWindow().setVisible(false);
                System.exit(0);
            }
        });
    }

    private void addNewMessageListenerTo(JButton btnSend) {
        btnSend.addActionListener(e -> {
            logger.debug("new msg event");
            chatPresenter.handleNewMessage();
        });
    }

    private void addShowHistoryListenerTo(JButton btnHistory) {
        btnHistory.addActionListener(e -> {
            logger.debug("show history event");
            chatPresenter.handleShowHistory();
        });
    }

    @Override
    public void appendMessage(String message) {
        messagesTextArea.setText(messagesTextArea.getText() + message + "\n");
    }

    @Override
    public void setMessages(List<String> messages) {
        StringBuilder msgs = new StringBuilder();
        messages.forEach(msg -> msgs.append(msg).append("\n"));
        messagesTextArea.setText(msgs.toString());
    }

    @Override
    public List<String> getMessages() {
        return Arrays.stream(messagesTextArea.getText().split("\n")).collect(Collectors.toList());
    }

    @Override
    public Optional<String> getNewMessageText() {
        return newMessageInput.getText().isEmpty() ? empty() : Optional.of(newMessageInput.getText());
    }

    @Override
    public void clearInputText() {
        newMessageInput.setText("");
    }

    @Override
    public void setWarning(String message) {
        StringBuilder wrnMsgs = new StringBuilder();
        warningLabel.setText(wrnMsgs.append(warningLabel.getText()).append("\n").toString());
        warningLabel.setBackground(Color.YELLOW);
    }

    @Override
    public void clearWarning() {
        warningLabel.setText("");
        warningLabel.setBackground(null);
    }

    @Override
    public void flashSuccess() {
        Color background = warningLabel.getBackground();
        warningLabel.setBackground(Color.GREEN);
        new Timer(1 * 1000, e -> warningLabel.setBackground(background))
                .start();
    }
}

