package ass.ekishigo.presenter;

import ass.ekishigo.model.MessageRepository;
import ass.ekishigo.model.Response;
import ass.ekishigo.view.ChatView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Observable;
import java.util.Observer;

import static ass.ekishigo.model.Response.Status.INTERNAL_WARN;
import static ass.ekishigo.model.Response.Status.OK;

/**
 * Created by igorekishev on 12.03.2017.
 * Project hw2
 *
 * @author Igor Ekishev
 * @email ekishigo@fel.cvut.cz
 */
public class ChatPresenter implements Observer {
    private static final Logger logger = LoggerFactory.getLogger(ChatPresenter.class.getSimpleName());
    private final MessageRepository msgRepo;
    private final ChatView chatView;

    @Inject
    public ChatPresenter(final MessageRepository msgRepo, final ChatView.Factory chatViewFactory) {
        this.msgRepo = msgRepo;
        this.chatView = chatViewFactory.create(this);
        this.msgRepo.addObserver(this);
    }

    public void handleNewMessage() {
        chatView.getNewMessageText().ifPresent(s -> {
            logger.debug("handling new msg");
            msgRepo.add(chatView.getNewMessageText().get());
            chatView.clearInputText();
        });
    }

    @Override
    public void update(Observable observable, Object arg) {
        if (arg instanceof Response) {
            Response response = (Response) arg;
            Response.Status status = response.getStatus();
            if (OK.equals(status)) {
                appendMsgAndFlashSuccess(response, chatView);
                logger.debug("view updated, status " + OK);
            } else if (INTERNAL_WARN.equals(status)) {
                appendMsgAndWarn(response, chatView);
                logger.debug("view updated, status " + INTERNAL_WARN);
            }
        } else {
            logger.error("Object arg is not instance of Response!");
            throw new IllegalStateException();
        }
    }

    private void appendMsgAndWarn(Response response, ChatView chatView) {
        chatView.setWarning(response.getStatus().getMsg());
        chatView.appendMessage(response.getData().orElse(INTERNAL_WARN.getMsg()));
    }

    private void appendMsgAndFlashSuccess(Response response, ChatView chatView) {
        response.getData().ifPresent(str -> {
            chatView.appendMessage(response.getData().get());
            chatView.clearWarning();
            chatView.flashSuccess();
        });
    }

    public void handleShowHistory() {
        chatView.setMessages(msgRepo.getAll());
    }
}
