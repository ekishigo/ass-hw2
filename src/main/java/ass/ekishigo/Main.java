package ass.ekishigo;

import ass.ekishigo.model.FileMessageRepository;
import ass.ekishigo.model.MessageRepository;
import ass.ekishigo.presenter.ChatPresenter;
import ass.ekishigo.view.ChatView;
import ass.ekishigo.view.SwingChatView;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import org.apache.log4j.BasicConfigurator;

import java.io.File;
import java.io.IOException;

/**
 * Created by igorekishev on 12.03.2017.
 * Project hw2
 *
 * @author Igor Ekishev
 * @email ekishigo@fel.cvut.cz
 */

public class Main {

    public static void main(String[] args) throws IOException {
        // configure log4j logger
        BasicConfigurator.configure();

        Injector injector = Guice.createInjector((Module) binder -> {
            binder.bind(MessageRepository.class).toInstance(
                    new FileMessageRepository(new File("messages.txt")));
            binder.bind(ChatPresenter.class);
            binder.install(new FactoryModuleBuilder()
                    .implement(ChatView.class, SwingChatView.class)
                    .build(ChatView.Factory.class));

        });
        ChatPresenter presenter = injector.getInstance(ChatPresenter.class);
        ChatPresenter presenter2 = injector.getInstance(ChatPresenter.class);
    }

} 
