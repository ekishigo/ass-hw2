package ass.ekishigo.presenter;

import ass.ekishigo.model.MessageRepository;
import ass.ekishigo.model.Response;
import ass.ekishigo.view.ChatView;
import com.google.common.collect.Lists;
import org.apache.log4j.BasicConfigurator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.*;

import static org.testng.Assert.assertEquals;

/**
 * Created by igorekishev on 13.03.2017.
 * Project hw2
 */
public class ChatPresenterTest {
    private static final String TEST_MESSAGE = "Test msg!";
    private static final List<String> HISTORY = Lists.newArrayList("One", "Two", "Three");
    private ChatPresenter chatPresenter;
    private ChatView mockView;
    private MessageRepository mockRepo;
    private File file;

    @BeforeMethod
    public void setUp() throws Exception {
        BasicConfigurator.configure();
        mockView = new MockChatView();
        file = new File("test.txt");
        file.createNewFile();
        mockRepo = new MockMessageRepository();
        chatPresenter = new ChatPresenter(mockRepo, chatPresenter -> mockView);
        mockRepo.addObserver(chatPresenter);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        mockView = null;
        mockRepo = null;
        chatPresenter = null;
        file.delete();
        file = null;
    }

    @Test
    public void newMessageStoredAndShowed() throws Exception {
        chatPresenter.handleNewMessage();
        assertEquals(mockView.getNewMessageText().get(), "");
        assertMessageIsWrittenToFile();
        assertMessageAppendedToMessageArea();
    }

    private void assertMessageAppendedToMessageArea() {
        assertEquals(mockView.getMessages().get(mockView.getMessages().size() - 1), TEST_MESSAGE);
    }

    private void assertMessageIsWrittenToFile() {
        assertEquals(mockRepo.getAll().get(mockRepo.getAll().size() - 1), TEST_MESSAGE);
    }

    @Test
    public void historyFetchedAndShowed() {
        chatPresenter.handleShowHistory();
        assertEquals(mockView.getMessages(), HISTORY);
    }

    private class MockChatView implements ChatView {
        String input = TEST_MESSAGE;
        List<String> msgs = new ArrayList<>();
        String warning = "";

        @Override
        public Optional<String> getNewMessageText() {
            return Optional.of(input);
        }

        @Override
        public void clearInputText() {
            input = "";
        }

        @Override
        public void appendMessage(String message) {
            msgs.add(message);
        }

        @Override
        public void setMessages(List<String> messages) {
            msgs = messages;
        }

        @Override
        public List<String> getMessages() {
            return msgs;
        }

        @Override
        public void setWarning(String message) {
            warning = message;
        }

        @Override
        public void clearWarning() {
            warning = "";
        }

        @Override
        public void flashSuccess() {
        }
    }

    private class MockMessageRepository extends Observable implements MessageRepository {
        List<String> msgs = HISTORY;

        @Override
        public boolean add(String message) {
            boolean add = msgs.add(message);
            setChanged();
            notifyObservers(new Response(Response.Status.OK, message));
            return add;
        }

        @Override
        public boolean clear() {
            msgs.clear();
            return true;
        }

        @Override
        public void addObserver(Observer observer) {
            super.addObserver(observer);
        }

        @Override
        public List<String> getAll() {
            return msgs;
        }
    }

}